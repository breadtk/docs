pymdown-extensions==9.1
mkdocs==1.2.3
mkdocs-git-revision-date-localized-plugin==0.11.1
mkdocs-material==8.1.7
mkdocs-material-extensions==1.0.3
markdown-include==0.6.0
mike==1.1.2